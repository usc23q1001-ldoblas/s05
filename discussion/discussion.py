class Person():
	
	def __init__(self):
		# protected attribute _name
		self._name = "John Doe"
		self._age = 20

	#setter method
	def set_name(self, name):
		self._name = name

	def get_name(self):
		print(f'Name of Person:{self._name}')
	

	def set_age(self, age):
		self._age = age

	def  get_age(self):
		print(f'Age:{self._age}')

# p1 = Person()
# p1.get_name()
# p1.set_name("Jane Smith")
# p1.get_name()
# p1.get_age()
# p1.set_age(40)
# p1.get_age()

#Inheritance
# The transfer of the characteristics of a parent class to child classes are dericed form it
# Parent-Child Relationship
# To create an inherited class 
class Employee(Person):
	def __init__(self,employeeID):
		#super() can be used to invoke the immediate parent class contructor
		super().__init__()
		# unique attribute to the Employee class
		self._employeeID = employeeID

		# EmployeeID Methods
	def get_employeeID(self):
		print(f'The Employee ID is{self._employeeID}')

	def set_employeeID(self, employeeID):
		self._employeeID = employeeID

	def get_details(self):
		print(f"{self._employeeID} belongs to {self._name}")

# emp1 = Employee("EMP001")
# emp1.set_name("Jane Smiiitthh")
# emp1.set_age(30)
# emp1.get_details()
# emp1.get_name()
# emp1.get_age()

class Student(Person):
	def __init__(self,studentNo,course,yearLvl):
		super().__init__()
		self._studentNo = studentNo
		self._course = course
		self._yearLvl = yearLvl

	def set_student(self, studentNo, course, yearLvl):
		self._studentNo = studentNo
		self._course = course
		self._yearLvl = yearLvl

	def get_student(self):
		print(f"{self._name} with student number {self._studentNo} is currently in year {self._yearLvl}  taking up {self._course}")

stud = Student("18100848","Computer Science", "4")
stud.set_name("Lester Doblas")
stud.get_student()	

#Polymorphism
# The mthod inherited from the parent class is not always fit for the child class. Re-implementaion/Overriding of method can be done in child class
# Ther are different methods to use polymorphism in Python

#Function and Objects 
# A function can be reated that can take any object, allowing polymorphism.
class Admin ():
	def is_admin(self):
		print(True)
	def user_type(self):
		print(f"Admin user ")

class Customer():
	def is_admin(self):
		print(False)
	def user_type(self):
		print(f"Customer user ")

#Define a test fuction that will take an object called obj.
def test_function(obj):
	obj.is_admin()
	obj.user_type()

user_admin = Admin()
user_customer = Customer()

test_function(user_admin)
test_function(user_customer)

# Polymorphism with Class Methods
# Python yses two different class types in the same way.
class TeamLead():
	def occupation(self):
		print('Team Lead')

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print('Team Member')

	def hasAuth(self):
		print(False)
tl1=TeamLead()
tm1=TeamMember()

for person in (tl1,tm1):
	person.occupation()


#Polymorphism with Inheritance
#Polymorphism in python definces methods in child class that have the same name as methods in the parent class.
#"Methods Overriding"

class  Zuitt():
	def tracks(self):
		print("we shape developers")
	def num_of_hours(self):
		print("learn web dev in 360 hours")
class DeveloperCareer(Zuitt):
	def num_of_hours(selfselfself):
		print("learn web dev in 24 hours!")
class PiShapedCareer(Zuitt):
	def num_of_hours(selfself):
		print("PiShaped will let you learn web dev in 500 hours!")
class ChourCourses(Zuitt):
	def num_of_hours(self):
		print("learn web dev in 2 hours!")

course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = ChourCourses()
course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()

#Abstraction
# an abstract class can be consideres as a blueprint for other class. 
#ABC = abstract Base Classes
# the import tells the program to get the abc module of python to be used 
from abc import ABC, abstractclassmethod

class Polygon(ABC):
	# Created an abstract methods called print_number_of_sides that needs to be inplemented by classes that will inherit Polygon class.
	@abstractclassmethod
	def print_number_of_sides(self):
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("this polygon has 3 sides")

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("this polygon has 5 sides")	

shape1=Triangle()
shape2=Pentagon()

shape1.print_number_of_sides()
shape2.print_number_of_sides()