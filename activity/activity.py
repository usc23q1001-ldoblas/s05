from abc import ABC, abstractclassmethod
class Animal(ABC):
	@abstractclassmethod
	def eat(self,food):
		self._food=food
		pass
	def make_sound(self):
		pass

class Dog(Animal):
	def __init__(self,name,breed,age):
		super().__init__()
		self._name=name
		self._breed=breed
		self._age=age

	def set_dog(self,name,breed,age):
		self._name=name
		self._breed=breed
		self._age=age

	def get_dog(self):
		print("Wehaa")

	#abstract methods
	def eat(self,food):
		self._food=food
		print(f"Eaten {self._food}")

	def make_sound(self):
		print("Arf! Bark! Woof!")

	def call(self):
		print(f"Here, {self._name} ")

class Cat(Animal):
	def __init__(self,name,breed,age):
		super().__init__()
		self._name=name
		self._breed=breed
		self._age=age

	def set_cat(self,name,breed,age):
		self._name=name
		self._breed=breed
		self._age=age

	def get_cat(self):
		print("Wehaa")

	#abstract methods
	def eat(self,food):
		self._food=food
		print(f"Eaten {self._food}")

	def make_sound(self):
		print("Miaw! Nyaw! Nyaaaa!")

	def call(self):
		print(f"{self._name}, come on! ")		

dog1 =Dog("Isis", "Dalmatian", 15)
dog1.eat("steak")
dog1.make_sound()
dog1.call()

cat1=Cat("Puss", "Persian", 4)
cat1.eat("tuna")
cat1.make_sound()
cat1.call()